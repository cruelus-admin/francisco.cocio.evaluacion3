package com.mycompany.francisco.cocio.evaluacion3;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Juneau
 */
@ApplicationPath("api")
public class AppConfig extends Application {
    
}

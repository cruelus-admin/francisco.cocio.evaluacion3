/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.evaluacion3.rest;

import com.mycompany.francisco.cocio.evaluacion3.dao.PersonajesJpaController;
import com.mycompany.francisco.cocio.evaluacion3.dao.exceptions.NonexistentEntityException;
import com.mycompany.francisco.cocio.evaluacion3.entity.Personajes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author francisco cocio
 */
@Path("personaje")
public class personajeRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response listaPersonajes() {
        PersonajesJpaController dao = new PersonajesJpaController();

        List<Personajes> lista = dao.findPersonajesEntities();

        return Response.ok(200).entity(lista).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Personajes personaje) {

        PersonajesJpaController dao = new PersonajesJpaController();

        try {
            dao.create(personaje);
        } catch (Exception ex) {
            Logger.getLogger(personajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(personaje).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Personajes personaje) {
        PersonajesJpaController dao = new PersonajesJpaController();
        try {
            dao.edit(personaje);
        } catch (Exception ex) {
            Logger.getLogger(personajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(personaje).build();

    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/(id_cadena)") 
    public Response eliminar(@PathParam("id_cadena") String id_cadena) {
        PersonajesJpaController dao = new PersonajesJpaController();
        try {
            dao.destroy(id_cadena);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(personajeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
     
        return Response.ok("Personaje Eliminado").build();
        
    }

}//end class

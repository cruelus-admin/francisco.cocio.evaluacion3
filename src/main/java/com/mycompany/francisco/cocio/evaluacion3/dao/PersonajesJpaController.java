/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.evaluacion3.dao;

import com.mycompany.francisco.cocio.evaluacion3.dao.exceptions.NonexistentEntityException;
import com.mycompany.francisco.cocio.evaluacion3.dao.exceptions.PreexistingEntityException;
import com.mycompany.francisco.cocio.evaluacion3.entity.Personajes;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author franc
 */
public class PersonajesJpaController implements Serializable {

    public PersonajesJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Personajes personajes) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(personajes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPersonajes(personajes.getIdCadena()) != null) {
                throw new PreexistingEntityException("Personajes " + personajes + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Personajes personajes) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            personajes = em.merge(personajes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = personajes.getIdCadena();
                if (findPersonajes(id) == null) {
                    throw new NonexistentEntityException("The personajes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Personajes personajes;
            try {
                personajes = em.getReference(Personajes.class, id);
                personajes.getIdCadena();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The personajes with id " + id + " no longer exists.", enfe);
            }
            em.remove(personajes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Personajes> findPersonajesEntities() {
        return findPersonajesEntities(true, -1, -1);
    }

    public List<Personajes> findPersonajesEntities(int maxResults, int firstResult) {
        return findPersonajesEntities(false, maxResults, firstResult);
    }

    private List<Personajes> findPersonajesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Personajes.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Personajes findPersonajes(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Personajes.class, id);
        } finally {
            em.close();
        }
    }

    public int getPersonajesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Personajes> rt = cq.from(Personajes.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.francisco.cocio.evaluacion3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franc
 */
@Entity
@Table(name = "personajes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personajes.findAll", query = "SELECT p FROM Personajes p"),
    @NamedQuery(name = "Personajes.findByIdCadena", query = "SELECT p FROM Personajes p WHERE p.idCadena = :idCadena"),
    @NamedQuery(name = "Personajes.findByAfiliacion", query = "SELECT p FROM Personajes p WHERE p.afiliacion = :afiliacion"),
    @NamedQuery(name = "Personajes.findByNombre", query = "SELECT p FROM Personajes p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Personajes.findByPlanetaOrigen", query = "SELECT p FROM Personajes p WHERE p.planetaOrigen = :planetaOrigen"),
    @NamedQuery(name = "Personajes.findByPrimeraAparicion", query = "SELECT p FROM Personajes p WHERE p.primeraAparicion = :primeraAparicion")})
public class Personajes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id_cadena")
    private String idCadena;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "afiliacion")
    private String afiliacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "planeta_origen")
    private String planetaOrigen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "primera_aparicion")
    private String primeraAparicion;

    public Personajes() {
    }

    public Personajes(String idCadena) {
        this.idCadena = idCadena;
    }

    public Personajes(String idCadena, String afiliacion, String nombre, String planetaOrigen, String primeraAparicion) {
        this.idCadena = idCadena;
        this.afiliacion = afiliacion;
        this.nombre = nombre;
        this.planetaOrigen = planetaOrigen;
        this.primeraAparicion = primeraAparicion;
    }

    public String getIdCadena() {
        return idCadena;
    }

    public void setIdCadena(String idCadena) {
        this.idCadena = idCadena;
    }

    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPlanetaOrigen() {
        return planetaOrigen;
    }

    public void setPlanetaOrigen(String planetaOrigen) {
        this.planetaOrigen = planetaOrigen;
    }

    public String getPrimeraAparicion() {
        return primeraAparicion;
    }

    public void setPrimeraAparicion(String primeraAparicion) {
        this.primeraAparicion = primeraAparicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCadena != null ? idCadena.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personajes)) {
            return false;
        }
        Personajes other = (Personajes) object;
        if ((this.idCadena == null && other.idCadena != null) || (this.idCadena != null && !this.idCadena.equals(other.idCadena))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.francisco.cocio.evaluacion3.entity.Personajes[ idCadena=" + idCadena + " ]";
    }
    
}
